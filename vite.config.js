import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import build from './config/build.js'

import { resolve } from "path";

import styleImport from 'vite-plugin-style-import';

function pathResolve(dir) {
  return resolve(__dirname, ".", dir);
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    styleImport({
      libs: [
        {
          libraryName: 'vant',
          esModule: true,
          resolveStyle: (name) => `vant/es/${name}/style/index`,
          resolveComponent: (name) => {
            return `vant/lib/${name}`;
          },
        },
      ],
    }),
  ],
  resolve: {
    alias: {
      "@": pathResolve("src"),
    }
  },
  build: build
})
