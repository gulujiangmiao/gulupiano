import Piano from '@/components/piano.vue'
import Export from '@/components/export.vue'
import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  { path: '/', component: Piano },
  { path: '/export', name: 'export', component: Export },
]

// const VueRouter = new VueRouter({
//   routes
// })

export default createRouter({
  history: createWebHashHistory(),
  routes,
})

