
export default {
  // 获取是否手机访问
  _isMobile() {
    let flag = navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i)
    return flag == null ? false : true;
  },
  // 格式化日期
  _dateFormat(date = "", format = "YMD_hms") {
    let dateTemp = date ? new Date(date) : new Date();
    let checkList = {
      "Y+": dateTemp.getFullYear(),
      "M+": this.numberFormat(dateTemp.getMonth() + 1),
      "D+": this.numberFormat(dateTemp.getDate()),
      "h+": this.numberFormat(dateTemp.getHours()),
      "m+": this.numberFormat(dateTemp.getMinutes()),
      "s+": this.numberFormat(dateTemp.getSeconds())
    };
    Object.keys(checkList).forEach(key => {
      format = format.replace(new RegExp(key), checkList[key]);
    });
    return format;
  },
  numberFormat(number){
    return number > 10 ? number : "0" + number;
  },
  // 划分数组
  _divArray(arr, num){
    let result = [];
    if(arr.length == 0){ return result; }
    let resulTemp = [];
    for(let i=0; i<arr.length; i++){
      resulTemp.push(arr[i]);
      if((i + 1) % num == 0){
        result.push(resulTemp);
        resulTemp = [];
      }
    }
    if(arr.length % num != 0){
      result.push(resulTemp);
    }
    return result;
  }
}