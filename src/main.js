import { createApp } from 'vue'
// import Vue from 'vue';
import App from './App.vue'
import router from '@/router-index.js'
// import animated from 'animate.css'

var config = {
  theme: "dark" // or light
}

const guluvue = createApp(App)
// const guluvue = createApp({
//   $config: config,
//   render: h => h(App)
// })
// const guluvue = new Vue({
//   config: config,
//   render: h => h(App),
// })

guluvue.$config = config

guluvue.use(router)
// guluvue.use(animated)

guluvue.mount('#app')